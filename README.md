# Rails App Boilerplate

Replace `the_commit` name by your project name

## Install

First do

    brew bundle

Then add this line to your profile file

    eval "$(direnv hook zsh)"


If you use ZSH + Oh-my-zsh, use [`zsh-nvm`](https://github.com/lukechilds/zsh-nvm) plugin:

    git clone https://github.com/lukechilds/zsh-nvm ~/.oh-my-zsh/custom/plugins/zsh-nvm

and add these lines before the `plugins` list is defined

    export NVM_DIR="$HOME/.nvm"
    export NVM_LAZY_LOAD=true

and add `zsh-nvm` to the `plugins` list


OR add these two lines to your profile file

    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm


Finally do

    direnv allow
    bundle install
    nvm install
    npm install


## Start the app

    foreman s

If you want to start the app an other way than the one that Heroku uses, duplicate `Procfile` to `Procfile.local`

    foreman s Procfile.local


## Optional features

 - Hutch
 - Redis
 - Rollout

To activate, un-comment the corresponding gem line in `Gemfile` and un-comment the initializer
