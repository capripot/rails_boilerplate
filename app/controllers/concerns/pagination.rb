module Concerns
  module Pagination
    def page
      [params[:page].to_i, 1].max
    end

    def per_page
      per_page = params[:per_page].to_i
      per_page > 0 ? [per_page, max_per_page].min : default_per_page
    end

    def default_per_page
      Kaminari.config.default_per_page
    end

    def max_per_page
      Kaminari.config.max_per_page
    end
  end
end
