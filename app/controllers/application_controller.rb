class ApplicationController < ActionController::Base
  include Concerns::Pagination

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from Satellite::AccessDenied do |exception|
    redirect_to failure_url message: exception.message
  end

  def show_interface?
    true
  end
  helper_method :show_interface?

end
