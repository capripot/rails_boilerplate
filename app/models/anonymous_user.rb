class AnonymousUser
  include Satellite::AnonymousUser

  def id
    0
  end

  def provider_key?(key)
    false
  end

  def admin?
    false
  end
end
