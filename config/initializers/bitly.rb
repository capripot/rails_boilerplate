if Figleaf::Settings.bitly.enabled?
  Bitly.use_api_version_3

  Bitly.configure do |config|
    config.api_version = 3
    config.access_token = Figleaf::Settings.bitly.api_key
  end
end
