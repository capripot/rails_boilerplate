Reimagine2.configure do |c|
  c.root_host = Figleaf::Settings.domain.root
  c.ssl_enabled = !Rails.env.development?
end
