class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :screen_name
      t.string :first_name
      t.string :last_name
      t.string :provider
      t.string :uid
      t.boolean :admin
      t.string :avatar

      t.timestamps null: false
    end
  end
end
